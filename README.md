# Hespul test

## Enoncé

Une installation photovoltaïque dispose d'un appareil appelé "datalogger" qui 
nous transmet chaque jour des données sur la production de la veille.

Cette installation photovoltaïque est composée de 2 onduleurs.

Les données sont transmises en format CSV. Elles contiennent la production 
horaire de chaque onduleur (cf fichiers de production dans le dossier `data`).

Le but principal est, à partir des fichiers de production, de disposer dans 
la base de données :

 - des données horaires de chaque onduleur
 - des données consolidées (somme des énergies) sur la journée pour l'ensemble 
   du système

De plus, proposer une interface web simple (pas de mise en forme demandée) qui 
permet :

 - d'envoyer un fichier CSV à charger
 - de voir la production totale d'une journée (au choix de l'utilisateur) pour 
   l'ensemble du système avec le total et la production par heure
   

## Résultat

Utilisation de Django 1.11 avec Python 3.6 :

    python3.6 -m venv env
    source env/bin/activate
    
    pip install -r requirements.txt
    
Tests :

    python manage.py test powerproduction
    
Lancement de l'application :

    python manage.py runserver
    
    
Résultat de `coverage`:

    Name                                                    Stmts   Miss  Cover
    ---------------------------------------------------------------------------
    hespul/__init__.py                                          0      0   100%
    hespul/settings.py                                         19      0   100%
    hespul/urls.py                                              3      0   100%
    hespul/wsgi.py                                              4      4     0%
    manage.py                                                  13      6    54%
    powerproduction/__init__.py                                 0      0   100%
    powerproduction/admin.py                                    1      0   100%
    powerproduction/apps.py                                     3      0   100%
    powerproduction/migrations/0001_initial.py                  7      0   100%
    powerproduction/migrations/0002_auto_20170420_2117.py       5      0   100%
    powerproduction/migrations/__init__.py                      0      0   100%
    powerproduction/models.py                                  36      0   100%
    powerproduction/tests.py                                  111      0   100%
    powerproduction/urls.py                                     3      0   100%
    powerproduction/views.py                                  116      0   100%
    ---------------------------------------------------------------------------
    TOTAL                                                     321     10    97%
