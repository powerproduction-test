from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^import/$', views.importdata, name='importdata'),
    url(r'^upload/$', views.upload, name='upload'),
    url(r'^results/$', views.results, name='results'),
    url(r'^result/(?P<date>[0-9]+)/$', views.result, name='result'),
    url(r'^truncate/$', views.truncate, name='truncate'),
    url(r'^test-update/$', views.test_update_of_hourly_data, name='testupdate')
]
