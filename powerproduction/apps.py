from django.apps import AppConfig


class PowerproductionConfig(AppConfig):
    name = 'powerproduction'
