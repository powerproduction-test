import datetime
from django.test import TestCase, Client
from powerproduction.models import Inverter, HourlyProduction, DailyProduction


# Create your tests here.
class TestModels(TestCase):
    def test_model_inverter(self):
        ref = 'ref'
        i = Inverter(ref=ref)
        i.save()
        self.assertEqual(i.ref, ref)
        self.assertEqual(str(i), ref)

    def test_model_hourlyproduction(self):
        dt = datetime.datetime(2017, 4, 22, 22, 0, 0)
        i, energy = Inverter(ref='1'), 200
        i.save()
        hp = HourlyProduction(inverter=i, datetime=dt, energy=energy)
        hp.save()
        self.assertEqual(hp.inverter, i)
        self.assertEqual(hp.datetime, dt)
        self.assertEqual(hp.energy, energy)
        self.assertIn(str(energy), str(hp))

    def test_model_dailyproduction(self):
        d = datetime.date(2017, 4, 22)
        i, energy = Inverter(ref='2'), 100
        i.save()
        dp = DailyProduction(inverter=i, date=d, energy=energy)
        dp.save()
        self.assertEqual(dp.inverter, i)
        self.assertEqual(dp.date, d)
        self.assertEqual(dp.energy, energy)
        self.assertIn(str(energy), str(dp))


class TestViewsEmptyDatabase(TestCase):
    def setUp(self):
        self.c = Client()

    def test_view_index(self):
        r = self.c.get('/')
        self.assertEqual(r.status_code, 200)
        self.assertIn(b'Power production', r.content)

    def test_view_results_nodata(self):
        r = self.c.get('/results/')
        self.assertEqual(r.status_code, 200)
        self.assertIn(b"No data", r.content)

    def test_view_import(self):
        r = self.c.get('/import/')
        self.assertEqual(r.status_code, 200)
        self.assertIn(b"Import data from local file", r.content)

    def test_view_upload_get(self):
        r = self.c.get('/upload/')
        self.assertEqual(r.status_code, 200)
        self.assertIn(b"Error when uploading file", r.content)

    def test_view_upload_post_nofile(self):
        r = self.c.post('/upload/')
        self.assertEqual(r.status_code, 200)
        self.assertIn(b"Error when uploading file", r.content)

    def test_view_upload_post_file_ok_but_missing_inverters(self):
        with open('./data/test_hespul_production_1.csv') as f:
            r = self.c.post('/upload/', {'name': 'datafile', 'datafile': f},
                            follow=True)
            self.assertEqual(r.status_code, 200)
            self.assertIn(b"Missing inverters in database: 1, 2", r.content)

    def test_view_upload_post_file_ok_but_empty_file(self):
        with open('./data/testing_empty_file.csv') as f:
            r = self.c.post('/upload/', {'name': 'datafile', 'datafile': f},
                            follow=True)
            self.assertEqual(r.status_code, 200)
            self.assertIn(b"Received an empty file", r.content)

    def test_view_upload_post_file_ok_but_missing_fields(self):
        with open('./data/testing_missing_field.csv') as f:
            r = self.c.post('/upload/', {'name': 'datafile', 'datafile': f},
                            follow=True)
            self.assertEqual(r.status_code, 200)
            self.assertIn(b"Bad data file", r.content)


class TestViewsDataInDatabase(TestCase):
    fixtures = ['inverter.json', 'hourlyproduction.json',
                'dailyproduction.json']

    def setUp(self):
        self.c = Client()

    def test_database(self):
        self.assertEqual(len(Inverter.objects.all()), 2)
        self.assertEqual(len(HourlyProduction.objects.all()), 96)
        self.assertEqual(len(DailyProduction.objects.all()), 4)

    def test_view_results_data(self):
        r = self.c.get('/results/')
        self.assertEqual(r.status_code, 200)
        self.assertIn(b"<table border=1 class='dataframe'>", r.content)

    def test_view_result_date_ok(self):
        r = self.c.get('/result/20170410/')
        self.assertEqual(r.status_code, 200)
        self.assertIn(b"Production of 2017-04-10 : 651.1 kWh", r.content)

    def test_view_result_data_nodata(self):
        r = self.c.get('/result/20170415/')
        self.assertEqual(r.status_code, 200)
        self.assertIn(b"No data for this date!", r.content)

    def test_view_upload_post_file_ok(self):
        with open('./data/test_hespul_production_1.csv') as f:
            r = self.c.post('/upload/', {'name': 'datafile', 'datafile': f},
                            follow=True)
            self.assertEqual(r.status_code, 200)
            self.assertIn(b"Data imported successfully!", r.content)

    def test_view_update_hourly_data(self):
        r = self.c.get('/test-update/', follow=True)
        self.assertEqual(r.status_code, 200)
        self.assertIn(b"Hourly production of inverter", r.content)


class TestViewsTruncate(TestCase):
    """Independant test case because we remove every production data."""
    fixtures = ['inverter.json', 'hourlyproduction.json',
                'dailyproduction.json']

    def setUp(self):
        self.c = Client()

    def test_view_truncate(self):
        # Before
        self.assertEqual(len(Inverter.objects.all()), 2)
        self.assertEqual(len(HourlyProduction.objects.all()), 96)
        self.assertEqual(len(DailyProduction.objects.all()), 4)

        r = self.c.get('/truncate/', follow=True)
        self.assertEqual(r.status_code, 200)
        self.assertIn(b"All power production data removed!", r.content)

        # After
        self.assertEqual(len(Inverter.objects.all()), 2)
        self.assertEqual(len(HourlyProduction.objects.all()), 0)
        self.assertEqual(len(DailyProduction.objects.all()), 0)
