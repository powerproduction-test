import datetime
import logging
import pandas
from pandas.parser import EmptyDataError
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.db.models import Q, Sum
from django.contrib import messages
from .models import Inverter, HourlyProduction, DailyProduction

# Log
log = logging.getLogger(__name__)


# Views
def index(request):
    """Main page."""
    log.debug("rendering index page")
    return render(request, 'powerproduction/index.html')


def truncate(request):
    """Remove all power production data from database."""
    HourlyProduction.objects.all().delete()
    DailyProduction.objects.all().delete()

    messages.add_message(request, messages.INFO,
                         'All power production data removed!')

    log.debug("remove all power production data from database ok")
    return HttpResponseRedirect(reverse('index'))


def test_update_of_hourly_data(request):
    """Update an hourly data to show if daily production is updated."""
    hp = HourlyProduction.objects.all().first()
    hp.energy += 1000
    hp.save()

    msg = (f'Hourly production of inverter {hp.inverter_id} at {hp.datetime} '
           f'updated to {hp.energy}...')
    messages.add_message(request, messages.INFO, msg)
    log.debug(msg)
    return HttpResponseRedirect(reverse('index'))


def importdata(request):
    """Form to import data from a file."""
    log.debug("rendering import data page")
    return render(request, 'powerproduction/import.html')


def upload(request):
    """Upload file into database."""

    def dateparser(s):
        """Date parser: return datetime object."""
        return datetime.datetime.strptime(s, '%d/%m/%y %H:%M')

    if request.method == 'POST' and 'datafile' in request.FILES:
        f = request.FILES['datafile']

        # Read csv with pandas
        try:
            df = pandas.read_csv(f)

        except EmptyDataError:
            error_message = "Received an empty file"
            log.error(error_message)
            return render(request, 'powerproduction/import.html', {
                'error_message': f"Error: {error_message}"
            })

        # Check the presence of 'identifier', 'datetime' and 'energy' columns
        columns = ['identifier', 'datetime', 'energy']
        if not all(e in df.columns for e in columns):
            error_message = "Bad data file (missing some important columns)"
            log.error(error_message)
            return render(request, 'powerproduction/import.html', {
                'error_message': f"Error: {error_message}"
            })

        # Parse 'datetime' column and create index
        df['datetime'] = df['datetime'].apply(dateparser)
        df.set_index(['identifier', 'datetime'], inplace=True)
        log.debug(f"read imported data file with pandas ok : {len(df)} rows")

        # Check if inverter is present Inverter table
        df_inverters = set(pandas.unique(df.reset_index()['identifier']))
        db_inverters = set(Inverter.objects.values_list('pk', flat=True))
        miss_inverters = df_inverters - db_inverters
        if miss_inverters:
            error_message = "Missing inverters in database: {}".format(
                ", ".join([str(e) for e in miss_inverters])
            )
            log.error(error_message)
            return render(request, 'powerproduction/import.html', {
                'error_message': f"Error: {error_message}"
            })

        # Import hourly data
        # -- first, remove existing data
        qs = [Q(inverter=inverter) & Q(datetime=dt)
              for (inverter, dt) in df.index.to_series().values]
        q = qs.pop(0)
        for e in qs:
            q |= e  # create Q queries (inverter1 & datetime1) | (...) | ...
        r = HourlyProduction.objects.filter(q).delete()
        log.debug(f"delete existing hourly data into database ({r})")

        # -- and import data into database
        recs = df.reset_index().to_dict('records')
        insts = [
            HourlyProduction(
                inverter=Inverter.objects.get(pk=rec['identifier']),
                datetime=rec['datetime'], energy=rec['energy'])
            for rec in recs
        ]
        r = HourlyProduction.objects.bulk_create(insts)
        log.debug(f"insert imported hourly data into database : {len(r)} rows")

        # Compute daily data
        df.reset_index(inplace=True)
        df['date'] = [e.date() for e in df['datetime']]
        dfj = df.groupby(['identifier', 'date']).sum()

        # -- first, remove existing data
        qs = [Q(inverter=inverter) & Q(date=date)
              for (inverter, date) in dfj.index.to_series().values]
        q = qs.pop(0)
        for e in qs:
            q |= e  # create Q queries (inverter1 & date1) | (...) | ...
        r = DailyProduction.objects.filter(q).delete()
        log.debug(f"delete existing daily data into database ({r})")

        # -- and import data into database
        recs = dfj.reset_index().to_dict('records')

        insts = [
            DailyProduction(
                inverter=Inverter.objects.get(pk=rec['identifier']),
                date=rec['date'], energy=rec['energy'])
            for rec in recs
        ]
        r = DailyProduction.objects.bulk_create(insts)
        log.debug(f"insert computed daily data into database : {len(r)} rows")

        messages.add_message(request, messages.INFO,
                             'Data imported successfully!')

        log.debug(f"redirection to index page")
        return HttpResponseRedirect(reverse('index'))

    else:
        log.error("error when uploading file (bad method or no file found)")
        return render(request, 'powerproduction/import.html', {
            'error_message': ("Error when uploading file (bad method or no "
                              "file found)")
        })


def results(request):
    """View results: list of available date."""
    # Sum of daily production
    sdps = (DailyProduction.objects.values('date')
            .annotate(energy=Sum('energy')).order_by('date'))
    context = {'sdps': sdps}
    log.debug("rendering results page")
    return render(request, 'powerproduction/results.html', context)


def result(request, date):
    """View results: detail of a specific day."""
    date = datetime.datetime.strptime(date, '%Y%m%d')
    log.debug(f"ask for detailed results for {date:%Y-%m-%d}")
    context = {'date': date, 'error_message': None}

    # Read data from database
    sumprod = sum(e.energy  # sum of production for the day
                  for e in DailyProduction.objects.filter(date=date))
    dps = DailyProduction.objects.filter(date=date)
    hps = (HourlyProduction.objects.filter(datetime__date=date)
           .order_by('datetime', 'inverter'))
    log.debug("reading datas from database ok")

    # Create HTML tables
    dfhps = pandas.DataFrame(list(hps.values()))
    if dfhps.empty:
        log.warning("missing data for this date")
        context['error_message'] = "No data for this date!"

    else:
        dfhps = dfhps.pivot(index='datetime', columns='inverter_id',
                            values='energy')
        del dfhps.index.name, dfhps.columns.name
        dfhps.columns = [f'inverter {e} (Wh)' for e in dfhps.columns]

        # Add sum at the end of the table
        dfdps = pandas.DataFrame(list(dps.values()))
        dfdps = dfdps.pivot(index='date', columns='inverter_id',
                            values='energy')
        del dfdps.index.name, dfdps.columns.name
        dfdps.columns = [f'inverter {e} (Wh)' for e in dfdps.columns]
        dfdps.index = ['sum (Wh)', ] * len(dfdps.index)

        tbhps = pandas.concat([dfhps, dfdps]).to_html()

        context['dps'] = dps
        context['tbhps'] = tbhps
        context['sumprod'] = sumprod

    log.debug("rendering result page")
    return render(request, 'powerproduction/result.html', context)
