import logging
from django.db import models
from django.db.models import Q

# Log
log = logging.getLogger(__name__)


# Models
class Inverter(models.Model):
    ref = models.CharField(max_length=100)  # reference

    def __str__(self):
        return self.ref


class HourlyProduction(models.Model):
    inverter = models.ForeignKey(Inverter, on_delete=models.CASCADE)
    datetime = models.DateTimeField()
    energy = models.IntegerField()

    class Meta:
        unique_together = (("inverter", "datetime"),)

    def __str__(self):
        return f'{self.energy} @ {self.inverter}, {self.datetime}'

    def save(self, *args, **kwargs):
        # First, compute the new daily production
        qfilter = Q(datetime__date=self.datetime.date(),
                    inverter=self.inverter)
        sum_prod = sum(HourlyProduction.objects.filter(qfilter)
                       .values_list('energy', flat=True))

        # Select the daily production to be updated
        qfilter = Q(date=self.datetime.date(), inverter=self.inverter)
        try:
            dp = DailyProduction.objects.get(qfilter)
            dp.energy = sum_prod
            dp.save()
            log.debug(f"Update daily production of {dp}")
        except DailyProduction.DoesNotExist:
            pass

        # Call the save method
        super(HourlyProduction, self).save(*args, **kwargs)


class DailyProduction(models.Model):
    inverter = models.ForeignKey(Inverter, on_delete=models.CASCADE)
    date = models.DateField()
    energy = models.IntegerField()

    class Meta:
        unique_together = (("inverter", "date"),)

    def __str__(self):
        return f'{self.energy} @ {self.inverter}, {self.date}'
